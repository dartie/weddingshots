package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gookit/color"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

/*
  Global variables
*/
var (
	__version__  string               = "1.0"
	__author__   string               = "Dario Necco"
	infoStyle    color.Style          // Colors
	successStyle color.Style          // Colors
	warningStyle color.Style          // Colors
	errorStyle   color.Style          // Colors
	logTag       = map[string]string{ // Log
		"INFO":    "[  INFO   ]",
		"SUCCESS": "[ SUCCESS ]",
		"WARNING": "[ WARNING ]",
		"ERROR":   "[  ERROR  ]",
	}
)

func myLog(text string, kind string, toFile bool) {
	kindUpper := strings.ToUpper(kind)
	switch kindUpper {
	case "INFO":
		infoStyle.Println(logTag[kindUpper], text)
	case "SUCCESS":
		successStyle.Println(logTag[kindUpper], text)
	case "WARNING":
		warningStyle.Println(logTag[kindUpper], text)
	case "ERROR":
		errorStyle.Println(logTag[kindUpper], text)
	}
}

func main() {
	/* Init colors */
	infoStyle = color.New()
	warningStyle = color.New(color.FgYellow, color.OpBold)
	successStyle = color.New(color.FgLightGreen, color.OpBold)
	errorStyle = color.New(color.FgLightRed, color.OpBold)

	/* Welcome message */
	myLog("Running version "+__version__, "info", false)

	/* Start server */
	router := gin.Default()

	router.LoadHTMLGlob("templates/*")
	router.Static("/static", "./static")
	//router.LoadHTMLFiles("templates/template1.html", "templates/template2.html")
	router.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.tmpl", gin.H{
			"title": "Wedding shots sharing",
		})
	})

	// Set a lower memory limit for multipart forms (default is 32 MiB)
	router.MaxMultipartMemory = 8 << 20 // 8 MiB
	router.POST("/upload", func(c *gin.Context) {
		// Multipart form
		form, _ := c.MultipartForm()
		files := form.File["uploadedFiles[]"]

		for _, file := range files {
			log.Println(file.Filename)

			// Upload the file to specific dst.
			dst := filepath.Join(".", "uploads", file.Filename)
			err := c.SaveUploadedFile(file, dst)
			if err != nil {
				errorStyle.Println(logTag["ERROR"], err)
				return
			}
		}
		c.String(http.StatusOK, fmt.Sprintf("%d files uploaded.\n\nThank you for your contribution!\nGrazie per il tuo contributo!", len(files)))
	})
	err := router.Run(":8081")
	if err != nil {
		myLog(fmt.Sprint(err), "error", true)
		os.Exit(19)
	}
}
